package webdriver;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class Tests {
    private final String URL="http://eds_university.eleks.com/login";

    @Test
    @Issue("UI-1")
    @Feature("UI functionality")
    @Description("Test verifies that button 'sing in' is available")
    public void Test1() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\juliaa_sha\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver(); //This line will start browser

        driver.get(URL);
        WebElement loginButton = driver.findElement(By.xpath("//*[@class='panel-body login-panel-body']/child::button"));
        loginButton.click();
        WebElement singInEmail = driver.findElement(By.xpath("//input[@id='email']"));
        singInEmail.sendKeys("shpintal06@gmail.com");
        WebElement singInPassword = driver.findElement(By.xpath("//input[@id='userPassword']"));
        singInPassword.sendKeys("VXj1w>hA9t");
        WebElement singInButton = driver.findElement(By.xpath("//*[@class='col-xs-12']/child::button"));
        singInButton.click();
    }

    @Test
    @Issue("UI-1")
    @Feature("UI functionality")
    @Description("Test verifies that button 'sing in' is available")
    public void Test2() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\juliaa_sha\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.get(URL);

        LogInPage logInPage = new LogInPage(driver);
        SingInPage singInPage = logInPage.singInPage();

        singInPage.fillInForm("shpintal06@gmail.com", "VXj1w>hA9t");
        singInPage.singInButton.click();
    }


    @Test
    @Issue("UI-1")
    @Feature("UI functionality")
    @Description("Test verifies that button 'sing in' incognito is available and wrong credentials do not work")
    public void Test3() throws InterruptedException, IOException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\juliaa_sha\\Downloads\\chromedriver_win32\\chromedriver.exe");


        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);

        WebDriver driver = new ChromeDriver(options);

        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        driver.get(URL);

        LogInPage logInPage = new LogInPage(driver);
        SingInPage singInPage = logInPage.singInPage();

        //task1
        singInPage.fillInForm("username@gmail.com", "password");

        assertTrue(singInPage.singInEmail.isDisplayed());
        assertTrue(singInPage.singInPassword.isDisplayed());
        assertTrue(singInPage.singInButton.isDisplayed());

        assertEquals("username@gmail.com", singInPage.singInEmail.getAttribute("value"));
        assertEquals("password", singInPage.singInPassword.getAttribute("value"));

        //task2
        singInPage.fillInForm("username2@gmail.com", "password2");

        assertEquals("username2@gmail.com", singInPage.singInEmail.getAttribute("value"));
        assertEquals("password2", singInPage.singInPassword.getAttribute("value"));

        //task3
        singInPage.singInEmail.clear();
        singInPage.singInPassword.clear();

        assertEquals("", singInPage.singInEmail.getAttribute("value"));
        assertEquals("", singInPage.singInPassword.getAttribute("value"));

        //task4
        TakesScreenshot screenshot =((TakesScreenshot)driver);
        File scrFile = screenshot.getScreenshotAs(OutputType.FILE);
        File destFile = new File("screenshot.png");

        FileUtils.copyFile(scrFile, destFile);

        //task6
        Actions action = new Actions(driver);
        action.sendKeys(singInPage.singInEmail,"shpintal06@gmail.com")
                .sendKeys(singInPage.singInPassword, "VXj1w>hA9t")
                .moveToElement(singInPage.singInPassword)
                .build()
                .perform();

        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].click();", singInPage.singInButton);

    }
}
